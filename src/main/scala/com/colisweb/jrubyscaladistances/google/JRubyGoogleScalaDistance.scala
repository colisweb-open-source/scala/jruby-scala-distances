package com.colisweb.jrubyscaladistances.google

import cats.MonadError
import cats.effect.IO
import com.colisweb.distances.model.path.DirectedPathWithModeAt
import com.colisweb.distances.model.{PathResult, Point, TravelMode}
import com.colisweb.distances.providers.google.TrafficModel.BestGuess
import com.colisweb.distances.providers.google.{
  GoogleDistanceDirectionsApi,
  GoogleDistanceDirectionsProvider,
  GoogleGeoApiContext
}
import com.colisweb.distances.{DistanceApi, Distances}
import com.colisweb.jrubyscaladistances.JRubyRedisConfiguration
import com.colisweb.simplecache.redis.circe.RedisCirceCache
import com.colisweb.simplecache.wrapper.cats.{CatsCache, SyncCatsCache}
import com.google.maps.OkHttpRequestHandler
import eu.timepit.refined.types.string.NonEmptyString
import io.circe.Codec
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration.FiniteDuration
import scala.util.Try

final class JRubyGoogleScalaDistance(googleApiConfig: GoogleApiConfiguration, redisConfig: JRubyRedisConfiguration) {

  val logger: Logger           = LoggerFactory.getLogger(classOf[OkHttpRequestHandler])
  val loggingF: String => Unit = (message: String) => logger.debug(message.replaceAll("key=([^&]*)&", "key=REDACTED&"))

  val googleGeoApiContext: GoogleGeoApiContext =
    GoogleGeoApiContext(
      NonEmptyString.unsafeFrom(googleApiConfig.apiKey),
      googleApiConfig.connectTimeout,
      googleApiConfig.readTimeout,
      googleApiConfig.queryRateLimit,
      loggingF
    )

  val distanceApi: DistanceApi[Try, DirectedPathWithModeAt] = {

    import io.circe.generic.extras.defaults._
    import io.circe.generic.extras.semiauto._

    implicit val distanceAndDurationCodec: Codec[PathResult] = deriveConfiguredCodec

    val redisTtl = redisConfig.expirationTimeout match {
      case duration: FiniteDuration => Some(duration)
      case _                        => None
    }

    val innerCache = new RedisCirceCache[DirectedPathWithModeAt, PathResult](redisConfig.getPool, redisTtl)(
      valueCodec = distanceAndDurationCodec
    )
    val catsCache: SyncCatsCache[Try, DirectedPathWithModeAt, PathResult] = CatsCache.sync(innerCache)

    Distances
      .from[Try, DirectedPathWithModeAt](
        GoogleDistanceDirectionsApi.sync(googleGeoApiContext, BestGuess)(
          GoogleDistanceDirectionsProvider.chooseMinimalDistanceRoute
        )
      )
      .caching(catsCache)
      .api
  }

  def getShortestDrivingDistance(
      origin: Point,
      destination: Point
  ): Try[PathResult] = getShortestDistance(origin, destination, TravelMode.Car())

  def getShortestDistance(
      origin: Point,
      destination: Point,
      travelMode: TravelMode
  ): Try[PathResult] = distanceApi.distance(DirectedPathWithModeAt(origin, destination, travelMode, None))

  def shutdown(): Unit = googleGeoApiContext.geoApiContext.shutdown()

}
