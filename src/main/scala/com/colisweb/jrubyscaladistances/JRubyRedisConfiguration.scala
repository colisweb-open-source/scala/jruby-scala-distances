package com.colisweb.jrubyscaladistances

import com.colisweb.simplecache.redis.RedisConfiguration
import redis.clients.jedis.JedisPool

import scala.concurrent.duration.Duration

final case class JRubyRedisConfiguration(
    host: String,
    port: Int,
    expirationTimeout: Duration
) {
  def getPool: JedisPool = {
    val configuration = new RedisConfiguration(
      host = host,
      port = port,
      timeout = expirationTimeout.toMillis.toInt,
      database = 1,
      password = None
    )
    RedisConfiguration.pool(configuration)

  }
}
