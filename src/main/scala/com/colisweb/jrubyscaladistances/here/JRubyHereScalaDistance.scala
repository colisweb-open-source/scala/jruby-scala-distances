package com.colisweb.jrubyscaladistances.here

import cats.effect.{IO, Sync}
import com.colisweb.distances.model.path.DirectedPathWithModeAt
import com.colisweb.distances.model.{PathResult, Point, TravelMode}
import com.colisweb.distances.providers.here.{HereRoutingApi, HereRoutingContext, RoutingMode}
import com.colisweb.distances.{DistanceApi, Distances}
import com.colisweb.jrubyscaladistances.JRubyRedisConfiguration
import com.colisweb.simplecache.redis.RedisConfiguration
import com.colisweb.simplecache.redis.circe.RedisCirceCache
import com.colisweb.simplecache.wrapper.cats.{CatsCache, SyncCatsCache}
import eu.timepit.refined.types.string.NonEmptyString
import io.circe.Codec

import scala.concurrent.duration.FiniteDuration
import scala.util.Try

final class JRubyHereScalaDistance(hereApiConfig: HereApiConfiguration, redisConfig: JRubyRedisConfiguration) {

  val hereContext: HereRoutingContext = HereRoutingContext(
    NonEmptyString.unsafeFrom(hereApiConfig.apiKey),
    hereApiConfig.connectTimeout,
    hereApiConfig.readTimeout
  )

  val distanceApi: DistanceApi[Try, DirectedPathWithModeAt] = {

    import io.circe.generic.extras.defaults._
    import io.circe.generic.extras.semiauto._

    implicit val distanceAndDurationCodec: Codec[PathResult] = deriveConfiguredCodec

    val redisTtl = redisConfig.expirationTimeout match {
      case duration: FiniteDuration => Some(duration)
      case _                        => None
    }
    val innerCache = new RedisCirceCache[DirectedPathWithModeAt, PathResult](redisConfig.getPool, redisTtl)(
      valueCodec = distanceAndDurationCodec
    )
    val catsCache: SyncCatsCache[Try, DirectedPathWithModeAt, PathResult] = CatsCache.sync(innerCache)

    Distances
      .from[Try, DirectedPathWithModeAt](HereRoutingApi.sync(hereContext)(RoutingMode.MinimalDistanceMode))
      .caching(catsCache)
      .api
  }

  def getShortestDrivingDistance(
      origin: Point,
      destination: Point
  ): Try[PathResult] = getShortestDistance(origin, destination, TravelMode.Car())

  def getShortestDistance(
      origin: Point,
      destination: Point,
      travelMode: TravelMode
  ): Try[PathResult] =
    distanceApi.distance(DirectedPathWithModeAt(origin, destination, travelMode, None))

}
